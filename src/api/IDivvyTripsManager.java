package api;

import java.time.LocalDate;

import model.data_structures.ILinkedList;
import model.data_structures.Queue;
import model.vo.VOBike;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {
	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 * @return 
	 */
	public boolean loadTrips();
	
	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 */
	public void loadStations();
	
	
	public void cargarDatos();
	
	public Queue<VOTrip> reqA1(int n, LocalDate fecha);
	
	/**
	 * Method to provide the number of loaded Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	int provideLoadedTripsNumber();
	
	/**
	 * Method to provide the number of loaded Divvy stations of the STS
	 * @param tripsFile - path to the file 
	 */
	int provideLoadedStationsNumber();

	public void loadJsonFile();

	public Queue<VOTrip> reqA2(int id);

	public Queue<VOTrip> reqA3(int n, LocalDate fecha);

	public Queue<VOBike> reqB4(int rangoInicial, int rangoFinal);

	public Queue<VOTrip> reqB5(String estacionDeInicio, String estacionDeLlegada, int rangoInicial, int rangoFinal);

	public String reqB6(String estacionDeInicio, String estacionDeLlegada);
	
	public double reqC7 (int m, int n, int o);

	

	
}
