package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {

		void add(T element);

		void delete(T element);

		public T get(T element);

		public int size(); 

		public T getElementByPosition(int pPosition);

		void listing();

		public T getCurrent(T element);

		public T next();

		void addLast(T element);

}

