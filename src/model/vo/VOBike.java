package model.vo;

public class VOBike implements Comparable <VOBike>
{
	
private int bikeId;
private int totalTrips;
private double distance;
private int duration;

public VOBike (int pBikeId, int pTotalTrips, double pDistance, int pDuration)
{
	bikeId = pBikeId;
	totalTrips = pTotalTrips;
	distance = pDistance;
	duration = pDuration;
}

public int darBikeId(){
	return bikeId;
}

public int darTotalTrips() {
	return totalTrips;
}

public double darDistance() {
	return distance;
}

public int darDuration() {
	return duration;
}

public void sumarTotalTrips()
{
	totalTrips++;
}

public void sumarTotalDistance( double pDistance)
{
	distance += pDistance;
}

public void sumarDuration(int pDuration)
{
	duration += pDuration;
}

@Override
public int compareTo(VOBike o) {
	
	int respuesta = -1;
	
	if (bikeId > o.darBikeId())
	{
		respuesta = 1;
	}
	
	else if (bikeId == o.bikeId)
	{
		respuesta = 0;
	}
	
	return respuesta;
}

}
