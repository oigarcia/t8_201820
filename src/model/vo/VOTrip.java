package model.vo;

import java.util.GregorianCalendar;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	//----------------------
	// Atributos
	//----------------------
	private int idTrip;
	private GregorianCalendar startTime;
	private GregorianCalendar endTime;
	private int bikeid ;
	private int tripDuration;
	private int fromStationId ;
	private String fromStationName;
	private int toStationId ;
	private String toStationName;
	private String usertype;
	private String gender ;
	private int birthyear;

	//----------------------
	// Constructor
	//----------------------
	
	public VOTrip ( int pIdTrip , GregorianCalendar pStartTime , GregorianCalendar pEndTime, int pBikeid, int pTripDuration, int pFromStation, String pFromStationName,int pToStationId, String pToStationName, String pUsertype, String pGender, int pBirthyear) {
		idTrip = pIdTrip;
		startTime = pStartTime ;
		endTime = pEndTime;
		bikeid = pBikeid;
	    tripDuration = pTripDuration;
		fromStationId = pFromStation;
		fromStationName = pFromStationName;
		toStationId = pToStationId;
		toStationName = pToStationName;
		usertype= pUsertype;
		gender = pGender;
		birthyear = pBirthyear;
	}
	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public int darIdTrip() {
		return idTrip;
	}	
	
	/** 
	 * @return id - Trip_id
	 */
	public GregorianCalendar darStartTime () {
		return startTime ;
	}	
	/** 
	 * @return id - Trip_id
	 */
	public GregorianCalendar darEndTime() {
		return endTime;
	}	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public int darBikeid() {
		return bikeid;
	}	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public int darTripDuration() {
		return tripDuration;
	}	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public int darFromStationId() {
		return fromStationId;
	}	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public String darFromStationName() {
		return fromStationName;
	}	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public int darToStationId() {
		return toStationId;
	}	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public String darToStationName() {
		return toStationName;
	}	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public String darUsertype() {
		return usertype;
	}	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public String darGender() {
		return gender;
	}	
	
	/** Retorna el id del Trip
	 * @return id - Trip_id
	 */
	public int darBirthyear() {
		return birthyear;
	}	
	
	
	
	

	
}
