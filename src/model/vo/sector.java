package model.vo;

import model.data_structures.Queue;

public class sector 
{
	private int numero;

	private double latitudMaxima;

	private double latitudMinima;

	private double longitudMaxima;

	private double longitudMinima;

	private Queue<VOStations> stations;

	public sector(int pNumSector ,double pLatMax,double pLatMin, double pLonMax, double pLonMin )
	{
		numero=pNumSector;
		latitudMaxima=pLatMax;
		latitudMinima=pLatMin;
		longitudMaxima=pLonMax;
		longitudMinima=pLonMin;
		stations=new Queue<VOStations>();
	}

	public int getNumSector() 
	{
		return numero;
	}

	public void setNumSector(int numSector) 
	{
		this.numero = numSector;
	}

	public double getLatMax() {
		return latitudMaxima;
	}

	public void setLatMax(double latMax) 
	{
		this.latitudMaxima = latMax;
	}

	public double getLatMin() {
		return latitudMinima;
	}

	public void setLatMin(double latMin) {
		this.latitudMinima = latMin;
	}

	public double getLongMax() {
		return longitudMaxima;
	}

	public void setLongMax(double longMax) {
		this.longitudMaxima = longMax;
	}

	public double getLongMin() {
		return longitudMinima;
	}

	public void setLongMin(double longMin)
	{
		this.longitudMinima = longMin;
	}


	public Queue<VOStations> getListaStations()
	{
		return stations;
	}
	
	public void addStations(VOStations v)
	{
		stations.enqueue(v);;
	}
	
	public boolean esta(double lat, double lon)
	{
		if(lat< latitudMaxima && lat > latitudMinima &&
				lon < longitudMaxima && lon > longitudMinima)
		{
			return true;
		}
		return false;
	}
	
	
	
}
