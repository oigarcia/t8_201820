package model.vo;

import java.util.GregorianCalendar;

public class VOStations implements Comparable<VOStations> {
	
	private int id ;
	private String stationName ;
	private String city ;
	private double latitude ;
	private double longitude ;
	private int dpCapacity ;
	private GregorianCalendar onlineDate ;
	private int puntos = 0;
	
	
	public VOStations ( int pId, String pStaionName, String pCity, double pLatitude, double pLongitude, int pCapacity, GregorianCalendar pOnlineDate) 
	{
		id= pId;
		stationName = pStaionName;
		city = pCity;
		latitude = pLatitude;
		longitude = pLongitude;
		dpCapacity = pCapacity;
		onlineDate = pOnlineDate;
		
		
	}
	
	
	/**
	 * @return id 
	 */
	public int id() {
		
		return id;
	}	
	
	/**
	 * @return station_name 
	 */
	public String stationName() {
		
		return stationName;
	}
	
	/**
	 * @return city
	 */
	public String city() {
	
		return city;
	}
	
	/**
	 * @return latitude.
	 */
	public double latitude() {
		
		return latitude;
	}

	/**
	 * @return longitude.
	 */
	public double longitude() {
	
		return longitude;
	}
	
	/**
	 * @return dpcapacity.
	 */
	public int dpcapacity() {
		
		return dpCapacity;
	}
	
	/**
	 * @return online date.
	 */
	public GregorianCalendar onlineDate() {
		
		return onlineDate;
	}
	
	public void sumarPuntos()
	{
		puntos ++;
	}
	
	public int darPuntos()
	{
		return puntos;
	}


	@Override
	public int compareTo(VOStations o) 
	{ int rta = 0;
		if(this.darPuntos()> o.darPuntos())
		{
			rta=1;
		}
		else if(this.darPuntos()< o.darPuntos())
		{
			rta=-1;
		}
		return rta;
	}
	
}
