package model.vo;

import model.data_structures.Queue;

public class VOBikeRoutes {
	
	private String sid;
	private String id;
	private String position;	            	        
	private String created_at ;
	private String created_meta ;
	private String updated_at ;
	private String updated_meta ;
	private String meta  ;
	private String bikeRoute ;
	private String the_geom ;
	private String type ;
	private String street ;
	private String fStreet ;
	private String tStreet ;
	private String status ;
	private String shape_Leng;
	private Queue<Coordenadas> coordenadas;
	
	public VOBikeRoutes (String pSid,String pId,String pPosition,String pCreatedAt,String pCreatedMeta,String pUpdated_at,String pUpdated_meta ,String pMeta , String pBikeRoute, String pThe_geom, String pType,String pStreet, String pFStreet,String pTStreet, String pStatus, String pShape_leng)
	{
		sid = pSid;
        id = pId;
        position = pPosition;	            	        
        created_at = pCreatedAt;
        created_meta = pCreatedMeta;
        updated_at = pUpdated_at;
        updated_meta = pUpdated_meta;
        meta = pMeta;
        bikeRoute =pBikeRoute;
        the_geom = pThe_geom;
        type = pType;
        street = pStreet;
        fStreet = pFStreet;
        tStreet =pTStreet;
        status = pStatus;
        shape_Leng =pShape_leng;
        coordenadas = new Queue();
	}
	
	
	public String darSid()
	{
		return sid;
	}
	public String darId()
	{
		return id;
	}
	public String darPosition ()
	{
		return position;
	}            	        
	public String darCreated_at ()
	{
		return created_at;
	}
	public String darCreated_meta ()
	{
		return created_meta;
	}
	public String darUpdated_at ()
	{
		return updated_at;
	}
	public String darUpdated_meta()
	{
		return updated_meta;
	}
	public String darMeta()
	{
		return meta;
	}
	public String darBikeRoute ()
	{
		return bikeRoute;
	}
	public String darThe_geom()
	{
		return the_geom;
	}
	public String darType()
	{
		return type;
	}
	public String darStreet ()
	{
		return street;
	}
	public String darFStreet ()
	{
		return fStreet;
	}
	public String darTStreet()
	{
		return tStreet;
	}
	public String darStatus ()
	{
		return status;
	}
	public String darShape_Leng ()
	{
		return shape_Leng;
	}
	
	public Queue<Coordenadas> darCoordenadas()
	{
		Queue<Coordenadas> cola = new Queue<Coordenadas>();
		String[] a =the_geom.split("\\)");
		String[] b = a[0].split("\\(");
		String[] c= b[1].split(",");
		for(int i=0; i< c.length; i++)
		{
			String[] d = c[i].split(" ");
			if(d.length==2)
			{
			Coordenadas	x = new Coordenadas( Double.parseDouble(d[1]),Double.parseDouble(d[0]) );
			cola.enqueue(x);
			}
			else
			{
				Coordenadas	x = new Coordenadas( Double.parseDouble(d[1]),Double.parseDouble(d[2]) );
				cola.enqueue(x);
			}
		}
		return cola;
	}
	
	
}
