package model.vo;

public class Coordenadas implements Comparable<Coordenadas>
{
	
	private double lat;
	private double lon;
	
	public Coordenadas(double la, double lo)
	{
		lat=la;
		lon=lo;
	}
	
	public double darLat()
	{
		return lat;
	}
	
	public double darLon()
	{
		return lon;
	}
	

	@Override
	public int compareTo(Coordenadas o) {
		// TODO Auto-generated method stub
		return 0;
	}
   
	
	
	
}
