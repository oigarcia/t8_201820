package model.logic;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Date;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Stack;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.sun.corba.se.impl.oa.poa.ActiveObjectMap.Key;
import com.sun.javafx.collections.SortableList;
import com.sun.org.apache.bcel.internal.generic.DDIV;

import api.IDivvyTripsManager;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import jdk.nashorn.internal.parser.JSONParser;
import model.data_structures.HashTableSCT;
import model.data_structures.LinkedList;
import model.data_structures.MaxPQ;
import model.data_structures.Queue;
import model.data_structures.RBTree;
import model.vo.VOBike;
import model.vo.Coordenadas;
import model.vo.VOBikeRoutes;
import model.vo.VOStations;
import model.vo.VOTrip;
import model.vo.sector;



public class DivvyTripsManager<JsonReader> implements IDivvyTripsManager {

	public static final String DIRECCION_ARCHIVO_1 = "./data/Divvy_Trips_2017_Q1.csv";
	public static final String DIRECCION_ARCHIVO_2 = "./data/Divvy_Trips_2017_Q2.csv";
	public static final String DIRECCION_ARCHIVO_3 = "./data/Divvy_Trips_2017_Q3.csv";
	public static final String DIRECCION_ARCHIVO_4 = "./data/Divvy_Trips_2017_Q4.csv";
	public static final String DIRECCION_ARCHIVO_5 =  "Data/Divvy_Stations_2017_Q1Q2.csv";
	public final static String DIRECCION_ARCHIVO_6 =  "Data/Divvy_Stations_2017_Q3Q4.csv";
	public static final String DIRECCION_ARCHIVO_7 = "./data/CDOT_Bike_Routes_2014_1216-transformed.json";

	private static final double EARTH_RADIUS = 6371;




	private HashTableSCT<Integer, VOStations> a1 = new HashTableSCT<>();
	private HashTableSCT<Integer, Queue<VOTrip>> a2 = new HashTableSCT<>();
	private HashTableSCT<String, RBTree<Integer, Queue <VOTrip>>> a3 = new HashTableSCT<>();
	private HashTableSCT<Integer, Queue<VOTrip>> c7 = new HashTableSCT<>();
	private HashTableSCT<Integer, sector> c8 = new HashTableSCT<>();
	private HashTableSCT<Integer, Queue<VOBikeRoutes>> c82 = new HashTableSCT<>();
	private HashTableSCT<Integer, Queue<VOStations>> c83 = new HashTableSCT<>();
	private RBTree<ChronoLocalDate, Queue<VOTrip>> arbol = new RBTree<>();
	private Queue<VOTrip> trips =  new Queue<>();
	private Queue<VOStations> stations = new Queue<>();
	private Queue <VOBikeRoutes> routes = new Queue<>();
	private RBTree <Integer,VOBike> bikeTree = new RBTree<>();
	private RBTree <Integer, VOBike> bikeTripDurationTree = new RBTree<>();
	

	private double latMax=0;
	private double latMin=1000000000;
	private double lonMax=0;
	private double lonMin=1000000000;



	/** 1C- Cargar toda la información de una fuente de datos seleccionada por el usuario y generar
	 ** las estructuras de datos necesarias.
	 */
	public boolean loadTrips () 
	{
		int contador = 0;
		boolean respuesta = true;
		String csvFile = "./data/Divvy_Trips_2017_Q1.csv";

		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			line = br.readLine();
			while ( (line=br.readLine()) != null) 
			{ 
				String[] datos = line.split(cvsSplitBy);

				int tripId = Integer.parseInt(datos[0].replace("\"",""));
				if(tripId == 15994906 )
				{
					break;
				}

				String[] d1 = datos[1].split("/");
				char[] parseo = d1[2].toCharArray();

				String startMes = d1[0].replace("\"","");
				String startDia = d1[1];
				String startAnio = (parseo[0]+ "") + (parseo[1]+ "") + (parseo[2]+ "") + (parseo[3]+ "");						
				String startHora = (parseo[5]+ "") + (parseo[6]+ "");
				String startMinuto = (parseo[8]+ "") + (parseo[9]+ "");
				String startSegundo = (parseo[11]+ "") + (parseo[12]+ "");


				String[] d2 = datos[2].split("/");
				char[] parseo2 = d2[2].toCharArray();
				String endMes = d2[0].replace("\"","");
				String endDia = d2[1];
				String endAnio = (parseo2[0]+ "") + (parseo2[1]+ "") + (parseo2[2]+ "") + (parseo2[3]+ "");
				String endHora = (parseo2[5]+ "") + (parseo2[6]+ "");
				String endMinuto = (parseo2[8]+ "") + (parseo2[9]+ "");
				String endSegundo = (parseo2[11]+ "") + (parseo2[12]+ "");

				String fecha = endMes + "/" + endDia + "/" + endAnio;


				GregorianCalendar startTime = new GregorianCalendar(Integer.parseInt(startAnio), Integer.parseInt(startMes), Integer.parseInt(startDia), Integer.parseInt(startHora), Integer.parseInt(startMinuto), Integer.parseInt(startSegundo));
				GregorianCalendar endTime   = new GregorianCalendar(Integer.parseInt(endAnio), Integer.parseInt(endMes), Integer.parseInt(endDia), Integer.parseInt(endHora), Integer.parseInt(endMinuto), Integer.parseInt(endSegundo));


				int bikeId = Integer.parseInt(datos[3].replace("\"",""));
				int tripDuration = Integer.parseInt(datos[4].replace("\"",""));
				int fromStationId = Integer.parseInt(datos[5].replace("\"",""));
				String fromStationName = datos[6];

				int toStationId = 0;
				String toStationName = "";
				String usertype= "";
				String gender = "";
				String year = "";
				int birthyear = 0;

				if(datos.length >= 6)
				{
					toStationId = Integer.parseInt(datos[7].replace("\"",""));
					toStationName = datos[8];
					usertype= datos[9];
					gender = datos[10];
					year = datos[11].replace("\"","");
					birthyear = 0;

					if(!year.isEmpty())
					{
						birthyear = Integer.parseInt(year);
					}
				}


				VOTrip newTrip = new VOTrip (tripId, startTime, endTime, bikeId, tripDuration, fromStationId, fromStationName , toStationId, toStationName,usertype ,gender , birthyear);
				trips.enqueue(newTrip);

				VOBike currentBike = bikeTree.get(bikeId);
				VOBike currentBikeTrip = bikeTripDurationTree.get(tripDuration);
				
				if (currentBike == null)
				{
					VOBike newBike = new VOBike(bikeId, 1, distanceCalculator (fromStationId, toStationId), tripDuration);
					bikeTripDurationTree.put(tripDuration, newBike);
					bikeTree.put(bikeId, newBike);
				}
				else
				{
					currentBike.sumarDuration(tripDuration);
					currentBike.sumarTotalDistance(distanceCalculator(fromStationId, toStationId));
					currentBike.sumarTotalTrips();
					currentBikeTrip.sumarDuration(tripDuration);
					currentBikeTrip.sumarTotalDistance(distanceCalculator(fromStationId, toStationId));
					currentBikeTrip.sumarTotalTrips();
				}
				
				String tripForHash = fromStationName + "-" + toStationName;
				RBTree<Integer, Queue<VOTrip>> treeB5 = a3.get(tripForHash);
				
				if(treeB5 == null)
				{
					RBTree<Integer, Queue<VOTrip>> treeB52 = new RBTree<>();
					Queue <VOTrip> colaA = treeB52.get(tripDuration);
					if (colaA == null)
					{
						Queue colaB = new Queue();
						colaB.enqueue(newTrip);
						treeB52.put(tripDuration, colaB);
					}
					else
					{
						colaA.enqueue(newTrip);
					}
						
				}
				else
				{
					treeB5.get(tripDuration).enqueue(newTrip);
					
					
				}
				
				

				LocalDate k= stringToLocalDate(fecha);

				Queue<VOTrip> lista = arbol.get(k);
				if(lista ==null)
				{
					Queue<VOTrip> listaNueva = new Queue<>();
					listaNueva.enqueue(newTrip);
					arbol.put( k, listaNueva);
				}
				else
				{
					lista.enqueue(newTrip);
				}

				////////////////////		


				int r = tripDuration/60;  //indice en el grupo
				while(r%2 !=0)
				{
					r++;          //comprobar que el minuto sea par
				}
				Queue<VOTrip> lista2 = a2.get(r);
				if(lista2 == null)
				{
					Queue<VOTrip> listaNueva2 = new Queue<>();
					listaNueva2.enqueue(newTrip);
					a2.put(r, listaNueva2);
				}
				else
				{
					lista2.enqueue(newTrip);
				}

				////////////////////
				//				VOStations stSalida = a1.get(fromStationId);
				//				stSalida.sumarPuntos();
				//
				//				VOStations stLlegada = a1.get(toStationId);
				//				stLlegada.sumarPuntos();
				//
				//				if(usertype.equals("Subscriber"))
				//				{
				//					stSalida.sumarPuntos();
				//					stLlegada.sumarPuntos();
				//				}

				int mes = Integer.parseInt(startMes);
				Queue<VOTrip> c = c7.get(mes);
				if(c==null)
				{
					Queue<VOTrip> c2 = new Queue<>();
					c2.enqueue(newTrip);
					c7.put(mes, c2);
				}
				else
				{
					c.enqueue(newTrip);
				}

				/////////////////////




				System.out.println(newTrip.darIdTrip() )  ;


			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			respuesta = false;
		} catch (IOException e) {
			e.printStackTrace();
			respuesta = false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return respuesta;
	}


	public static LocalDate stringToLocalDate(String pfecha)
	{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");		
		//convert String to LocalDate
		LocalDate localDate = LocalDate.parse(pfecha, formatter);
		return localDate;
	}




	@Override
	public int provideLoadedTripsNumber() {

		return trips.size();
	}



	@Override
	public int provideLoadedStationsNumber() {

		return stations.size();
	}


	public void loadStations() 
	{

		boolean respuesta = true;
		String csvFile = "./data/Divvy_Stations_2017_Q1Q2.csv";
		BufferedReader br = null;
		String line = "";
		//Se define separador ","
		String cvsSplitBy = ",";
		try {

			br = new BufferedReader(new FileReader(csvFile));
			line = br.readLine();
			while ( (line=br.readLine()) != null) 
			{ 
				String[] datos = line.split(cvsSplitBy);


				String id = datos[0].replace("\"",""); ;
				String stationName = datos[1].replace("\"","");
				String city = datos[2].replace("\"","");
				String latitude = datos[3].replace("\"","");
				String longitude = datos[4].replace("\"","");
				String dpCapacity = datos[5].replace("\"","");

				String[] d6 = datos[6].split("/");
				char[] parseo = d6[2].toCharArray();
				String mes = d6[0].replace("\"","");
				String dia = d6[1];
				String anio = (parseo[0]+ "") + (parseo[1]+ "") + (parseo[2]+ "") + (parseo[3]+ "");					
				String hora = (parseo[5]+ "") + (parseo[6]+ "");
				String minuto = (parseo[8]+ "") + (parseo[9]+ "");
				String segundo = (parseo[11]+ "") + (parseo[12]+ "").replace("\"","");


				GregorianCalendar onlineDate = new GregorianCalendar(Integer.parseInt(anio),Integer.parseInt(mes), Integer.parseInt(dia), Integer.parseInt(hora),Integer.parseInt(minuto), Integer.parseInt(segundo));					


				VOStations newStation = new VOStations  (Integer.parseInt(id), stationName, city, Double.parseDouble(latitude),Double.parseDouble(longitude), Integer.parseInt(dpCapacity), onlineDate);
				stations.enqueue(newStation);;

				System.out.println(newStation.id() )  ;
				//////////////////////
				int lat = Integer.parseInt(latitude);
				int lon = Integer.parseInt(longitude);
				if(lat < latMin)
				{
					latMin = lat ;

				}
				if(lat > latMax)
				{
					latMax = lat ;

				}
				if(lon < lonMin)
				{
					lonMin = lon ;

				}
				if(lon > lonMax){
					lonMax = lon ;

				}




			}
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			respuesta = false;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			respuesta = false;
		} 
		finally 
		{
			if (br != null) 
			{
				try 
				{  br.close(); } 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}


	public void loadJsonFile() 
	{

		try
		{
			JsonParser parser = new JsonParser();
			JsonArray arr = (JsonArray) parser.parse(new FileReader(DIRECCION_ARCHIVO_5));

			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				String datos [] = null;
				JsonObject obj = (JsonObject) arr.get(i);
				String sid ="";
				String id = "";
				String position ="";	            	        
				String created_at ="";
				String created_meta = "";
				String updated_at = "";
				String updated_meta = "";
				String meta = "";
				String bikeRoute ="";
				String the_geom = "";
				String type = "";
				String street = "";
				String fStreet = "";
				String tStreet ="";
				String status = "";
				String shape_Leng ="";

				if(obj.get("sid")!= null)
					sid = obj.get("sid").getAsString();

				if(obj.get("id")!= null)
					id = obj.get("id").getAsString();

				if(obj.get("position")!= null)
					position = obj.get("position").getAsString();

				if(obj.get("created_at")!= null)
					created_at = obj.get("created_at").getAsString();


				try
				{
					if(obj.get("created_meta")!= null)
						created_meta = obj.get("created_meta").getAsString();
				}
				catch(Exception e)
				{
					created_meta = "";
				}


				if(obj.get("updated_at")!= null)
					updated_at = obj.get("updated_at").getAsString();

				try
				{
					if(obj.get("updated_meta")!= null)
						updated_meta = obj.get("updated_meta").getAsString();
				}
				catch(Exception e)
				{
					updated_meta = "";
				}

				if(obj.get("meta")!= null)
					meta = obj.get("meta").getAsString();

				if(obj.get("BIKEROUTE")!= null)
					bikeRoute = obj.get("BIKEROUTE").getAsString();

				if(obj.get("the_geom")!= null)
					the_geom = obj.get("the_geom").getAsString();

				if(obj.get("TYPE")!= null)
					type = obj.get("TYPE").getAsString();

				if(obj.get("STREET")!= null)
					street = obj.get("STREET").getAsString();

				if(obj.get("F_STREET")!= null)
					fStreet = obj.get("STREET").getAsString();
				try{
					JsonElement element = obj.get("T_STREET");
					if(element != null)
						tStreet = obj.get("T_STREET").getAsString();
				}
				catch(Exception e){

					tStreet = "CALLE FALSA 123";
				}


				if(obj.get("STATUS")!= null)
					status = obj.get("STATUS").getAsString();

				if(obj.get("Shape_Leng")!= null)
					shape_Leng = obj.get("Shape_Leng").getAsString();

				VOBikeRoutes newRoute = new VOBikeRoutes(sid, id, position, created_at, created_meta, updated_at, updated_meta, meta, bikeRoute, the_geom, type, street, fStreet, tStreet, status, shape_Leng);

				routes.enqueue(newRoute);

				System.out.println(newRoute.darId() + "-" + i);

			}
		}

		catch (JsonIOException e1 ) {

			e1.printStackTrace();

		}

		catch (JsonSyntaxException e2) {

			e2.printStackTrace();

		}

		catch (FileNotFoundException e3) {
			e3.printStackTrace();
		}

		System.out.println("Inside loadServices with " + DIRECCION_ARCHIVO_5);

	}



	public double distance(double latitud,double longitud,double endLat, double endLong)
	{
		double dLat  = Math.toRadians((endLat - latitud));
		double dLong = Math.toRadians((endLong - longitud));

		latitud = Math.toRadians(latitud);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(latitud) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c; // <-- d
	}
	public static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}

	public void cargarDatos()
	{
		loadJsonFile();
		loadStations();
		loadTrips();
	}

	/** 1A. Buscar	los	viajes	que	terminaron	en	una	estación	con	capacidad	mayor	o	igual	a	“n”	
dada	la	fecha	“d”	(día/mes/año)	de	terminación. Crear	un	índice	(con	
un	árbol	balanceado)	sobre	los	viajes	por	 fecha	de	 terminación,	y	un	índice	 (con	una	
tabla	 de	 hash)	 sobre	 las	 estaciones	 por	 su	 id.	 Dichos	 índices	 deben	 ser	 creados	 al	
momento	de	cargar	los	datos.*/
	public Queue<VOTrip> reqA1(int n, LocalDate fecha)
	{
		Queue<VOTrip> resp = new Queue<>();
		Iterator<VOTrip> iter = arbol.get(fecha).iterator();

		while(iter.hasNext())
		{
			VOTrip actual = (VOTrip)iter.next(); 
			int id = actual.darToStationId();
			VOStations actual2 = a1.get(id);


			if(actual2.dpcapacity()>=n)
			{
				resp.enqueue(actual);
			}

		}
		return resp;
	}

	public double distanceCalculator(int pStationId1, int pStationId2)
	{
		double  distance = 0;
		VOStations startingStation = a1.get(pStationId1) ;
		VOStations endStation = a1.get(pStationId2);
		distance = distance(startingStation.latitude(), startingStation.longitude(), endStation.latitude(), endStation.longitude());
		return distance;
	}

	/** 2A. Buscar	los	viajes	que	tienen	duraciones	similares. Crear	una	Tabla	de	Hash	que	divida	los	viajes	de	la	siguiente	forma:	Grupo	1:	de	0	a	2	
minutos;	Grupo	2:	de	2	a	4	minutos;	Grupo	3:	de	4	a	6	minutos	y	así	sucesivamente.	
Esto	significa	que	para	este	requerimiento	tener	un	tiempo	similar	implica	estar	en	el	
mismo	grupo. */ 
	public Queue <VOTrip> reqA2(int id)
	{

		while(id%2 !=0)
		{
			id ++;          //comprobar que el minuto de consulta sea par
		}
		return a2.get(id/2);

	}


	/** 3A. Buscar	los	viajes	con	los	recorridos	más	largos	en	una	fecha	(día/mes/año)
Identificar	los	n viajes	que	recorren más	distancia	en	una	fecha	dada,	retornando	una	
lista	ordenada	por	dicha	distancia	(en	metros).	La	distancia	de	un	viaje	se	va	a	estimar	
como	la	 distancia	 “harvesiana”	entre	 sus	estaciones.	Utilice	 un	heap	para	almacenar	
todos	 los	 viajes. Los	 valores	 de	 número	 de	 viajes	 y	 fecha	 son	 recibidos	 como	
parámetro.
	 */
	public Queue <VOTrip> reqA3(int n, LocalDate fecha)
	{ 
		Queue<VOTrip> cola = new Queue<>();
		cola = arbol.get(fecha);

		Merge ordenar = new Merge();
		Comparable[]  array = ordenar.parseArrayQU(cola);
		ordenar.sort(array);
		cola = ordenar.parseQueue((Comparable[]) array); 

		Queue <VOTrip> rta = new Queue <VOTrip>();
		for(int i =0; i<cola.size() && i<n;i++)
		{
			rta.enqueue(cola.dequeue());
		}

		return rta;
	}

	/**4B. Buscar	bicicletas	para	mantenimiento.
Crear	 un	 índice	 sobre	 las	 bicicletas	 (con	 un	 árbol	 balanceado)	 dado	 su	 tiempo	 de	
recorrido	total	(suma	de	todos	los	tiempos	de	sus	viajes	en	segundos).
Usar	el	índice	anterior	para	permitir	la	buscar	las	bicicletas	que	 tienen	un	 tiempo	de	
recorrido	 total	 dentro	 de	 un	 rango	 que	 entra	 como	 parámetro.	
	 */
	public Queue <VOBike> reqB4(int rangoInicial, int rangoFinal)
	
	{	Queue <VOBike> bicicletasMantenimiento = new Queue <VOBike>();
		
		Iterable<Integer> llaves= bikeTripDurationTree.keys(rangoInicial, rangoFinal);
		Iterator<Integer> iter = llaves.iterator();
		
		while (iter.hasNext())
		{
			bicicletasMantenimiento.enqueue(bikeTripDurationTree.get(iter.next()));
		}
	
		return bicicletasMantenimiento ;
	}

	/** 5B. Búsqueda	de	viajes	por	estaciones	de	salida	y	llegada	en	un	rango	de	tiempo.
Agrupar	los	datos	por	la	estación	de	inicio	y	de	terminación	usando	una	tabla	de	hash	
(ayuda:	 la	 llave	 puede	 ser:	 “nombre	 estación	 inicio”	 +	 “-”	 +	 “nombre	 estación	
terminación”).	 Luego	 para	 cada	 entrada	 generar	 un	 árbol	 ordenado,	 por	 tiempo	 de	
recorrido	en	segundos,	con	los	viajes	correspondientes.
	 */
	public Queue <VOTrip> reqB5(String estacionDeInicio, String estacionDeLlegada, int limiteInferiorTiempo, int limiteSuperiorTiempo)
	{
		Queue<VOTrip> respuesta5B = new Queue<VOTrip>();
		String llaveString = estacionDeInicio +"-" + estacionDeLlegada;
	
		RBTree<Integer, Queue<VOTrip>> tree5B = a3.get(llaveString);
		
		Iterable<Integer> llaves2 = tree5B.keys(limiteInferiorTiempo, limiteSuperiorTiempo);
		Iterator<Integer> iter = llaves2.iterator();
		
		while (iter.hasNext())
		{
			Queue<VOTrip> variableCola5B= tree5B.get(iter.next());
			Iterator<VOTrip> iter2 = variableCola5B.iterator();
			while (iter2.hasNext())
			{
				respuesta5B.enqueue(iter2.next());
			}
		}
		return respuesta5B;
	}

	/** 6B. Búsqueda	de	hora	pico de	viajes	por	estaciones	de	salida	y	llegada	
Identificar	la	franja	de	hora	entera	(ejemplo:	2:00pm	a	3:00	pm	o	5:00	pm	a	6:00	pm)	
en	las	 que	 hubo	más	viajes,	 para	 una	 ruta	 determinada	 por	 una	estación	 de	inicio	y	
una	estación	de	llegada.
	 */	
	public String reqB6 (String estacionDeInicio, String estacionDeLlegada)
	{
		String respuesta = "";
		RBTree<Integer, Queue<VOTrip>> tree6B = a3.get(estacionDeInicio + "-" + estacionDeLlegada);
		Queue<VOTrip> respuesta6B = reqB5(estacionDeInicio, estacionDeLlegada, tree6B.min(), tree6B.max());
		
		HashTableSCT<Integer,Queue <VOTrip>> a4 = new HashTableSCT<>();
		
		Iterator<VOTrip> iter = respuesta6B.iterator();
		while (iter.hasNext())
		{
			VOTrip viajeActual = iter.next();
			GregorianCalendar variable1 = viajeActual.darStartTime();
			GregorianCalendar variable2 = viajeActual.darEndTime();
			int horaSalidaViaje = variable1.get(Calendar.HOUR_OF_DAY);;
			int horaLlegadaViaje = variable2.get(Calendar.HOUR_OF_DAY);;
			
			Queue<VOTrip> tree6B2 = tree6B.get(horaSalidaViaje);
			
			if(tree6B2 == null)
			{
				Queue cola6B = new Queue();
				cola6B.enqueue(viajeActual);
				a4.put(horaSalidaViaje, cola6B);
			}
			else
			{
				tree6B2.enqueue(viajeActual);
			}
			
			Queue<VOTrip> tree6B3 = tree6B.get(horaLlegadaViaje);
			
			if(tree6B2 == null)
			{
				Queue cola6B2 = new Queue();
				cola6B2.enqueue(viajeActual);
				a4.put(horaLlegadaViaje, cola6B2);
			}
			else
			{
				tree6B3.enqueue(viajeActual);
			}
			
		}
		
		int hora = 0;
		for (int i = 0; i < a4.size(); i++)
		{
			int tamanio = 0;
			
			
			if (a4.get(i).size()>tamanio)
			{
				tamanio = a4.get(i).size();
				hora = i;
			}
			
		}
		respuesta = "La franja de hora en la que hubo más viajes es a las " + hora +  ":00 horas.";
		return respuesta;
	}




	/**
	 *  7C Campaña	de	publicidad.	
• Por	cada	viaje	que	empieza	en	la	estación	gana	1	Punto	
• Por	cada	viaje	que	termina	en	la	estación	gana	1	Punto	
• Si	el	viaje,	además	 de	 cumplir	alguna	 de	las	 condiciones	anteriores,	lo	 realizó	 un	
usuario	de	tipo	“Subscriber”	gana	1	Punto	más.
Posteriormente	 implemente	 un	 método	 que	 retorne	 cuanto	 debería	 cobrar	 Divvy	 a	
una	 empresa	 si	 está	 dispuesta	 a	 pagar	 “x”	 dólares	 por	 punto	 y	 quiere	 poner	 su	
publicidad	durante	un	mes	entre	 febrero	y	noviembre de	2018	en	las	“n”	estaciones	
donde	hay	mayor	impacto.		“x”, “n”,	y	el	mes son	valores	que	ingresan	como	datos	de	
entrada.	
Pista:	Realice	un	ordenamiento	de	las	estaciones	por	puntos	usando	heapsort.	
El	resultado	debe	mostrar	el	nombre	de	las	“n”	estaciones	y	el	dinero	total	a	cobrar.
	 */
	public double reqC7(int m, int n, int o)
	{
		Queue<VOTrip> anterior= c7.get(m-1);	
		Queue<VOTrip> act= c7.get(m);
		Queue<VOTrip> siguiente= c7.get(m+1);

		Queue<VOStations> rta = new Queue();
		int stSalidaTotal = 0;
		int stLlegadaTotal = 0;

		Iterator it1= anterior.iterator();
		while(it1.hasNext())
		{
			VOTrip actual = (VOTrip)it1.next();

			VOStations stSalida = a1.get(actual.darFromStationId());
			stSalida.sumarPuntos();

			VOStations stLlegada = a1.get(actual.darToStationId());
			stLlegada.sumarPuntos();

			if(actual.darUsertype().equals("Subscriber"))
			{
				stSalida.sumarPuntos();
				stLlegada.sumarPuntos();
			}
			stSalidaTotal += stSalida.darPuntos();
			stLlegadaTotal += stLlegada.darPuntos();
		}


		Iterator it2= act.iterator();
		while(it2.hasNext())
		{
			VOTrip actual = (VOTrip)it2.next();

			VOStations stSalida = a1.get(actual.darFromStationId());
			stSalida.sumarPuntos();

			VOStations stLlegada = a1.get(actual.darToStationId());
			stLlegada.sumarPuntos();

			if(actual.darUsertype().equals("Subscriber"))
			{
				stSalida.sumarPuntos();
				stLlegada.sumarPuntos();
			}
			stSalidaTotal += stSalida.darPuntos();
			stLlegadaTotal += stLlegada.darPuntos();
		}


		Iterator it3= siguiente.iterator();
		while(it3.hasNext())
		{
			VOTrip actual = (VOTrip)it3.next();

			VOStations stSalida = a1.get(actual.darFromStationId());
			stSalida.sumarPuntos();

			VOStations stLlegada = a1.get(actual.darToStationId());
			stLlegada.sumarPuntos();

			if(actual.darUsertype().equals("Subscriber"))
			{
				stSalida.sumarPuntos();
				stLlegada.sumarPuntos();
			}
			stSalidaTotal += stSalida.darPuntos();
			stLlegadaTotal += stLlegada.darPuntos();
		}


		MaxPQ<VOStations> heap = new MaxPQ<>();
		rta.enqueue(heap.delMax());


		int rtaF = n*o*stLlegadaTotal*stSalidaTotal;
		return   rtaF;


	}


	public int darSector(double lat, double lon)
	{
		for(int i=1; i< c8.size(); i++)
		{ 
			if(c8.get(i).esta(lat, lon)){
				return i;
			}
		}
		return 0;
	}




	/** 8. Sectorización
Se	 necesita	 identificar	 las	 estaciones	 Divvy	 ubicadas	 cerca	 de	 un	 punto	 geográfico	
dado.	Con	esta	 finalidad	se	crearán	LA	x	LO sectores	 rectangulares,	cada	uno	con	un	
y	dos	longitudes. Donde	LA	corresponde	al	número	de	divisiones	en	latitud	y	LO	al	número	de	divisiones	en	longitud.
Para	 definir	 esta	 sectorización	 se	 deben	 tomar	 las	 localizaciones	 geográficas	 de	 las	
estaciones	y	las	localizaciones	geográficas	del	recorrido	de	todas	las	ciclorutas.	
Hacer	 un	 método	 que	 retorne	 la	 longitud	 máxima,	 longitud	 mínima,	 latitud	 máxima	 y	
latitud	mínima.
	 */public Queue <VOStations> reqC8 (int latitud, int longitud)
	 {
		 double alto = (latMax-latMin)/latitud;
		 double ancho = (lonMax-lonMin)/longitud;
		 int num =1;
		 for(int i=0; i<alto; i++)
		 {
			 for(int j=0; j<ancho; j++)
			 {
				 sector nuevo = new sector(num, latMin+alto, latMin, lonMin+ancho, lonMin);
				 c8.put(num, nuevo);
				 num++;
				 lonMin += ancho;

			 }
			 latMin += alto;
		 }

		 Iterator it1 = routes.iterator();
		 while(it1.hasNext())
		 {
			 VOBikeRoutes actual = (VOBikeRoutes) it1.next();
			 Iterator it2 = actual.darCoordenadas().iterator();
			 while(it2.hasNext())
			 {
				 Coordenadas act = (Coordenadas) it2.next();
				 int s = darSector(act.darLat(),act.darLon());

				 Queue<VOBikeRoutes> lista = c82.get(s);
				 if(lista == null)
				 {
					 Queue<VOBikeRoutes> listaNueva = new Queue<>();
					 listaNueva.enqueue(actual);
					 c82.put(s, listaNueva);
				 }
				 else
				 {
					 lista.enqueue(actual);
				 }
			 }

		 }

		 Iterator it3 =stations.iterator();
		 while(it3.hasNext())
		 {
			 VOStations ac = (VOStations) it3.next();
			 int t = darSector(ac.latitude(), ac.longitude());

			 Queue<VOStations> lista = c83.get(t);
			 if(lista == null)
			 {
				 Queue<VOStations> listaNueva2 = new Queue<>();
				 listaNueva2.enqueue(ac);
				 c83.put(t, listaNueva2);
			 }
			 else
			 {
				 lista.enqueue(ac);
			 }

			
		 }
		 Queue rta= new Queue();
		 rta.enqueue(c8);
		 rta.enqueue(c82);
		 rta.enqueue(c83);
		 return 	rta;

	 }



	 /**9. Búsqueda	georreferenciada	(latitud	y	longitud)	de	estaciones	
	 Usando	la	sectorización	(punto	8)	y	a	partir	de	una	localización	(latitud,	longitud)	dada	
	 determine	el	sector	en	el	que	está	e	informe	las	estaciones	que	se	encuentran	en	el	
	 sector. Este	 método	 debe	 mostrar	 el	 número	 del	 sector	 de	 consulta,	 y	 para	 las	 estaciones	
	 resultantes	 hay	 que	 informar	 su	 Id,	 nombre,	 su	 localización	 geográfica	 (latitud,	
	 longitud)	y	su	distancia	en	metros	a	la localización	de	consulta.
	 Si	no	hay	estaciones	en	el	sector	resultante	o	si	la	localización	de	consulta	No	está	al	
	 interior	de	un	sector	debe	informarse	esta	situación.	
	 1. Cree	una	tabla	de	hash	que	agrupe	las	estaciones	por	su	sector
	 2. Para	calcular la	distancia	en	metros	entre	2	localizaciones	(latitud,	longitud)	use	la	
	 distancia	harvesiana	(harvesine	formule).
	 	  */
	 public Queue <VOStations> reqC9 (double pLatitud, double pLongitud)
	 {
		 int numero = darSector(pLatitud, pLongitud);
		 Queue<VOStations> cola9C = c83.get(numero);
		 return cola9C;
		 
	 }


	 	 /**10. Búsqueda	georreferenciada	(latitud	y	longitud)	de	ciclorutas
	 Usando	la	sectorización	(punto	8),	a	partir	de	una	localización	(latitud,	longitud)	dada	
	 determine	el	sector	en	el	que	está	e	informe	las	ciclorutas	que	atraviesan	el	sector.
	 Este	 método	 debe	 mostrar	 el	 número	 del	 sector	 de	 consulta;	 asi	 mismo,	 para	 las	
	 ciclorutas	 resultantes	 hay	 que	 informar	 su	 calle	 de	 referencia	 (STREET)	 y	 la(s)	
	 localización(es)	 geográficas	 de	 su	 recorrido	 que	 atraviesan	 el	 sector	 de	 consulta	
	 (latitud,	longitud	en	el	atributo	“the_geom”).
	 Si	no	hay	ciclorutas que	atraviesen	el	sector	de	consulta	o	si	la	localización	de	consulta	
	 No	está	al	interior	de	un	sector	debe	informarse	esta	situación.
	 1. Cree	 una	 tabla	 de	 hash	 que	 tenga	 como	 llaves	 los	 sectores	 y	 como	 valores	 unas	
	 listas	con	las	ciclorrutas	que	pasan	por	cada	sector.
	 	  */

	 public Queue <VOBikeRoutes> reqC10 (double pLatitud, double pLongitud)
	 {
		 int numero = darSector(pLatitud, pLongitud);
		 Queue<VOBikeRoutes> cola10C = c82.get(numero);
		 
		 Iterator<VOBikeRoutes> iter = cola10C.iterator();
		
		 while (iter.hasNext())
		 {
			 VOBikeRoutes ruta = iter.next();
			 double lat = ruta.darCoordenadas().dequeue().darLat();
			 double lon = ruta.darCoordenadas().dequeue().darLon();
			 double distancia = distance(pLatitud, pLongitud,lat, lon );
			 System.out.println("La distancia entre las ciclorrutas es de " + distancia);
		 }
		 
		 return cola10C;
		 
	 }

	 	/**11. Búsqueda	de	ciclorutas	para	hacer	viaje
	 Usando	la	sectorización	 (punto	8)	y	dado	una	localización	de	inicio	y	una	localización	
	 final (arbitrarias).	Se	quiere	buscar	las	ciclorutas	que	se	podrían	usar	para	hacer	el	viaje	entre	
	 ambas	 localizaciones.	 Para	 simplificar	 se	 supondrá	 que	 una	 cicloruta	 solo	 se	 puede	
	 usar	 si	 al	 menos	 una	 de	 las	 localizaciones	 de	 su	 recorrido	 está	 en	 el	 sector	 de	 la	
	 localización	de	inicio	y	una	de	las	localizaciones	de	su	recorrido	está	en	el	sector	de	la	
	 localización	final.
	 Por	cada	cicloruta	resultante	se	debe	mostrar	el	id	de	la	cicloruta,	el	nombre	de	la	calle	
	 de	 referencia	 (“STREET”)	 y	 la	 distancia	 total	 para	 hacer	 el	 recorrido.
	  */

		 
		 public Queue <VOBikeRoutes> reqC11(double latitudI, double longitudI, double latitudF, double longitudF)
			{
			 Queue <VOBikeRoutes> auxInicial = reqC10(latitudI, longitudI);
			 Queue <VOBikeRoutes> auxFinal = reqC10(latitudF, longitudF);
			 Queue <VOBikeRoutes> resp = new Queue <VOBikeRoutes>();		 
		

				for (int i = 0; i < auxInicial.size(); i++) 
				{
					Iterator<VOBikeRoutes> iter1 = auxInicial.iterator();
					VOBikeRoutes actualInicial = iter1.next();
					
					for (int j = 0; j <auxFinal.size(); j++)
					{
						Iterator<VOBikeRoutes> iter2 = auxFinal.iterator();
						VOBikeRoutes actualFinal = iter2.next();
						
						
						if(actualInicial.darId().equals(actualFinal.darId()))
						{
							if(resp == null)
							{
								resp.enqueue(actualInicial);
							}
						}
					}
				}
				return resp;

			}
	 }




	 /**Estructura tomada a partir de https://algs4.cs.princeton.edu/22mergesort/
	  *  @author Robert Sedgewick & Kevin Wayne
	  *  @modifiedBy Andrea Montoya S. & Oscar García. 
	  */
	 class Merge {

		 private Object[] array;

		 public Merge()
		 {
			 //array = parseArray(qu);

		 }

		 public Comparable[] parseArrayQU(Queue cola)
		 {
			 Comparable toSort[] = new Comparable[cola.size()];
			 Iterator<Object> iter = cola.iterator();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
			 int i = 0;
			 while(iter.hasNext())
			 {
				 Comparable actual = (Comparable)iter.next();

				 if(actual != null)
				 {
					 toSort[i] = actual;
				 }
				 i++;
			 }

			 return toSort;
		 }


		 public Queue parseQueue(Comparable []a)
		 {
			 Queue cola = new Queue();
			 for(int i = 0; i < a.length;i++)
			 {
				 if(a[i] != null)
				 {
					 cola.enqueue(a[i]);
				 }
			 }
			 return cola;
		 }



		 public Comparable[] sort(Comparable[] a) 
		 { 
			 Comparable[] aux = new Comparable[a.length]; 
			 sort(a, aux, 0, a.length - 1); 
			 return a;
		 } 


		 private  void sort(Comparable[] a, Comparable[] aux, int lo, int hi) 
		 { 
			 if (hi <= lo) return; 
			 int mid = lo + (hi - lo) / 2; 
			 sort(a, aux, lo, mid); 
			 sort(a, aux, mid+1, hi); 
			 merge(a, aux, lo, mid, hi); 
		 } 

		 private  void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi)
		 { 
			 for (int k = lo; k <= hi; k++)
				 aux[k] = a[k]; 
			 int i = lo, j = mid+1;
			 for (int k = lo; k <= hi; k++) 
			 { 
				 if      (i > mid)              a[k] = aux[j++];
				 else if (j > hi)               a[k] = aux[i++];
				 else if (less(aux[j], aux[i])) a[k] = aux[j++];
				 else                           a[k] = aux[i++]; 
			 } 
		 } 

		 private  boolean less(Comparable v, Comparable w) {
			 return v.compareTo(w) < 0;
		 }

}


