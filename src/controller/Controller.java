package controller;

import java.time.LocalDate;

import api.IDivvyTripsManager;
import model.data_structures.ILinkedList;
import model.data_structures.Queue;
import model.logic.DivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();

	public static void loadStations() 
	{
           manager.loadStations();
	}

	public static void loadTrips() 
	{
		 manager.loadTrips();
	}
	
	public static void loadJsonFile() 
	{
		 manager.loadJsonFile() ;
	}
	
	public static void cargarDatos()
	{
		manager.cargarDatos();
	}
	
	public static Queue<VOTrip> reqA1(int n, LocalDate fecha)
	{
		return manager.reqA1(n, fecha);
	}
	
	public static Queue<VOTrip> reqA2(int id)
	{
		return manager.reqA2(id);
	}
	
	public static Queue<VOTrip> reqA3(int n, LocalDate fecha)
	{
		return manager.reqA3(n, fecha);
	}
	
	public static Queue<VOBike> reqB4(int rangoInicial, int rangoFinal)
	{
		return manager.reqB4(rangoInicial, rangoFinal);
	}
	
	public static Queue<VOTrip> reqB5(String estacionDeInicio, String estacionDeLlegada, int rangoInicial, int rangoFinal)
	{
		return manager.reqB5(estacionDeInicio, estacionDeLlegada, rangoInicial, rangoFinal);
	}
	
	public static String reqB6(String estacionDeInicio, String estacionDeLlegada)
	{
		return manager.reqB6(estacionDeInicio, estacionDeLlegada);
	}
	
	public static double reqC7(int m, int n, int o)
	{
		return manager.reqC7(m, n, o);
	}
	
	public static int loadedTripsTotal() {
		return manager.provideLoadedTripsNumber();
	}

	public static int loadedStationsTotal() {
		return manager.provideLoadedStationsNumber();
	}
}
