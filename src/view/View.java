package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.Queue;
import model.logic.DivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOTrip;

public class View {

	public static void main(String[] args){

		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		Controller controlador = new Controller();
		int option;
		int n;
		int rangoInicial;
		int rangoFinal;
		int rangoInicialB5 = 0;
		int rangoFinalB5 = 0;
		String estacionDeInicioB5 = "";
		String estacionDeLlegadaB5 = "";
		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();
			option = linea.nextInt();
			n = 0;
			rangoInicial =0;
			rangoFinal =0;
			switch(option)
			{

			case 0:  //Carga de datos
				String dataTrips = "";  // ruta del archivo de Trips
				String dataStations = ""; // ruta del archivo de Stations
				String dataBikeRoutes = ""; // ruta del archivo de ciclorutas
				dataTrips = DivvyTripsManager.DIRECCION_ARCHIVO_1 + ":" + DivvyTripsManager.DIRECCION_ARCHIVO_2 +":" + DivvyTripsManager.DIRECCION_ARCHIVO_3 + ":" + DivvyTripsManager.DIRECCION_ARCHIVO_4;
				dataStations = DivvyTripsManager.DIRECCION_ARCHIVO_5 + ":" + DivvyTripsManager.DIRECCION_ARCHIVO_6;
				dataBikeRoutes = DivvyTripsManager.DIRECCION_ARCHIVO_7;

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.currentTimeMillis();

				Controller.cargarDatos();

				//TODO
				System.out.println("Total trips cargados en el sistema: �XXX?");
				//TODO
				System.out.println("Total estaciones cargadas en el sistema: �YYY?");
				//TODO
				System.out.println("Total ciclorutas cargadas en el sistema: �ZZZ?");

				//Tiempo en cargar
				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");
				break;


			case 1: //Req A1

				//Capacidad
				System.out.println("Ingrese la capacidad de la estaci�n: (Ej: 56)");
				int capacidad = linea.nextInt();

				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq1A = linea.next();


				// Datos Fecha inicial
				LocalDate localDateInicio = convertirFecha(fechaInicialReq1A);

				//Metodo 1A
				Queue<VOTrip> viajesA1 = controlador.reqA1(capacidad, localDateInicio);
				for (int i = 0; i < viajesA1.size(); i++) 
				{
					
					  System.out.println(viajesA1.dequeue().darIdTrip() 
					  + viajesA1.dequeue().darFromStationId()
					  + viajesA1.dequeue().darFromStationName()
					  + viajesA1.dequeue().darToStationName() 
					  + viajesA1.dequeue().darStartTime() 
					  + viajesA1.dequeue().darEndTime());
					
				}
				break;

				case 2: //Req A2

				System.out.println("Ingrese la duraci�n deseada:");
				//Duraci�n de los viajes
				n = Integer.parseInt(linea.next());
				Queue<VOTrip> viajesA2 = controlador.reqA2(n);

				for(int i = 0; i < viajesA2.size(); i++)
				{
					  System.out.println(viajesA2.dequeue().darIdTrip() 
							  + viajesA2.dequeue().darFromStationId()
							  + viajesA2.dequeue().darFromStationName()
							  + viajesA2.dequeue().darToStationName() 
							  + viajesA2.dequeue().darTripDuration()
							  + viajesA2.dequeue().darStartTime() 
							  + viajesA2.dequeue().darEndTime());
				}
				
				
				break;

				case 3: //Req A3

				//N�mero de viajes que se desan buscar
				System.out.println("Ingrese el n�mero de viajes a buscar: ");
				n = Integer.parseInt(linea.next());


				//Fecha
				System.out.println("Ingrese la fecha (Ej : 28/3/2017)");
				String fechaInicialReq3A = linea.next();

				// Datos Fecha
				LocalDate localDateInicio3A = convertirFecha(fechaInicialReq3A);

				//Metodo A3
				Queue<VOTrip> viajesPorBicicleta = controlador.reqA3(n, localDateInicio3A);
				for(int i = 0; i < viajesPorBicicleta.size(); i++)
				{
					  System.out.println(viajesPorBicicleta.dequeue().darIdTrip() 
							  + viajesPorBicicleta.dequeue().darFromStationId()
							  + viajesPorBicicleta.dequeue().darFromStationName()
							  + viajesPorBicicleta.dequeue().darToStationName()
							  + viajesPorBicicleta.dequeue().darStartTime() 
							  + viajesPorBicicleta.dequeue().darEndTime());
				}
				break;

			case 4: //Req B4

				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo minimo total recorrido: ");
				rangoInicial = Integer.parseInt(null, linea.nextInt());

				//Tiempo mánimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo máximo total recorrido: ");
				rangoFinal = Integer.parseInt(null, linea.nextInt());


				//Metodo B1
				Queue<VOBike> bikesB1 = controlador.reqB4(rangoInicial,rangoFinal);

				for(int i = 0; i < bikesB1.size(); i++)
				{
					  System.out.println(bikesB1.dequeue().darBikeId() 
							  + bikesB1.dequeue().darDistance()
							  + bikesB1.dequeue().darDuration()
							  + bikesB1.dequeue().darTotalTrips());
				}
				break;

			case 5: //Req B5

				//Estacion de inicio
				System.out.println("Ingrese la estación de inicio (Ej : Shedd Aquarium)");
				String estacionDeInicio1 = linea.next();

				//Estacion de llegada
				System.out.println("Ingrese la estación de terminación (Ej : Shedd Aquarium)");
				String estacionDeLlegada1 = linea.next();

				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo minimo (en segundos) del recorrido: ");
				rangoInicial = linea.nextInt();

				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo m�ximo (en segundos) del recorrido: ");
				rangoFinal = linea.nextInt();


				//Req B5
				Queue<VOTrip> viajesB5 = controlador.reqB5(estacionDeInicioB5, estacionDeLlegadaB5 ,rangoInicialB5, rangoFinalB5);
				for(int i = 0; i < viajesB5.size(); i++)
				{
					  System.out.println(viajesB5.dequeue().darBikeid()
							  + viajesB5.dequeue().darBirthyear()
							  + viajesB5.dequeue().darFromStationId()
							  + viajesB5.dequeue().darFromStationName()
							  + viajesB5.dequeue().darGender()
							  + viajesB5.dequeue().darIdTrip()
							  + viajesB5.dequeue().darToStationId()
							  + viajesB5.dequeue().darTripDuration()
							  + viajesB5.dequeue().darUsertype());
				}
				break;

			case 6: //Req B6
				//Estacion de inicio
				System.out.println("Ingrese la estación de inicio (Ej : Shedd Aquarium)");
				String estacionDeInicio = linea.next();

				//Estacion de llegada
				System.out.println("Ingrese la estación de llegada (Ej : Shedd Aquarium)");
				String estacionDeLlegada = linea.next();

				System.out.println(controlador.reqB6(estacionDeInicio, estacionDeLlegada));
				break;

			//case 7: //Req C1

				//System.out.println("Ingrese cuanto estaria dispuesto a pagar por punto (Ej: 3.5):");
				//double valorPorPunto = Double.parseDouble(linea.next());

				//System.out.println("Ingrese el número de estaciones donde se quiere poner publicidad (Ej: 4):");
				//int numEstacionesConPublicidad = Integer.parseInt(linea.next());

				//System.out.println("Ingrese un mes entre Febrero y Noviembre (Ej: 3):");
				//int mesCampanna = Integer.parseInt(linea.next());

				//double res = Controller.reqC7(valorPorPunto, numEstacionesConPublicidad, mesCampanna);

				//System.out.println("Total a pagar: " + res.costoTotal);
				//System.out.println("Estaciones");

				//for(Station t : res.estaciones)
				//{
				//	System.out.print(t.getStationName());
				//}
				//break;

				//case 8: //Req C2

				//System.out.println("Ingrese el numero de divisiones en latitud (LA)");
				//int LA = Integer.parseInt(linea.next());

				//System.out.println("Ingrese el numero de divisiones en longitud (LO)");
				//int LO = Integer.parseInt(linea.next());

				//double [] rectangulo = controlador.C2(LA, LO);
				//System.out.println("latitud máxima: " + rectangulo[0]);
				//System.out.println("longitud máxima: " + rectangulo[1]);
				//System.out.println("latitud mínima: " + rectangulo[2]);
				//System.out.println("longitud mínima: " + rectangulo[3]);
				//break;

				//case 9: //Req C3
				//System.out.println("Ingrese una latitud: (74.33)");
				//double latitud = Double.parseDouble(linea.next());

				//System.out.println("Ingrese una longitud: (-4.33)");
				//double longitud = Double.parseDouble(linea.next());

				//int sector = controlador.darSector(latitud, longitud);
				//System.out.println("Sector: " + sector);

				//System.out.println("Estaciones: ");
				//ILista<Station> estacionesCercanas = controlador.C3(latitud, longitud);
				//for(Station s: estacionesCercanas) {
					//System.out.print("id: " + s.getStationId() + ", " );
					//System.out.print("nombre:" + s.getStationName() + ", ");

					//TODO imprimir localizacion y distancia en metros a la posicion de entrada
				//}
				//break;


			//case 10: //Req C4
				//System.out.println("Ingrese una latitud: (74.33)");
				//double latitudR = Double.parseDouble(linea.next());

				//System.out.println("Ingrese una longitud: (-4.33)");
				//double longitudR = Double.parseDouble(linea.next());

				//int sectorR = controlador.darSector(latitudR, longitudR);
				//System.out.println("Sector: " + sectorR);

				//System.out.println("Ciclorutas: ");
				//ILista<BikeRoute> ciclorutasCercanas = controlador.C4(latitudR, longitudR);
				//for(BikeRoute b: ciclorutasCercanas) {
					//System.out.print("Calle de referencia:" + b.getReferenceStreet() + ", ");
					//TODO imprimir los otros atributos que se piden en el enunciado
				//}
				//break;

			//case 11: //Req C5
				//System.out.println("Ingrese una latitud inicial: (74.33)");
				//double latitudI = Double.parseDouble(linea.next());

				//System.out.println("Ingrese una longitud inicial: (-4.33)");
				//double longitudI = Double.parseDouble(linea.next());

				//System.out.println("Ingrese una latitud final: (74.33)");
				//double latitudF = Double.parseDouble(linea.next());

				//System.out.println("Ingrese una longitud final: (-4.33)");
				//double longitudF = Double.parseDouble(linea.next());

				//System.out.println("Ciclorutas: ");
				//ILista<BikeRoute> ciclorutasQueSePuedenUsar = controlador.C5(latitudI, longitudI, latitudF, longitudF);
				//for(BikeRoute b: ciclorutasQueSePuedenUsar) {
					//System.out.print("Calle de referencia:" + b.getReferenceStreet() + ", ");
					//TODO imprimir los otros atributos que se piden en el enunciado
				//}
				//break;

			case 12: //Salir
				fin = true;
				linea.close();
				break;
			}
		}
	}

	private static void printMenu()
	{
		System.out.println("-----------------ISIS 1206 - Estructuras de Datos------======----");
		System.out.println("-------------------- Proyecto 2   - 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("0. Cargar datos de todos los archivos");

		System.out.println("\nParte A:\n");
		System.out.println("1. Viajes que terminaron en una estación con cierta capacidad, en una fecha dada (1A)");
		System.out.println("2. Viajes con duraciones similares (2A)");
		System.out.println("3. Viajes con los recorridos más largos en una fecha dada (3A)");

		System.out.println("\nParte B:\n");
		System.out.println("4. Bicicletas para mantenimiento (1B)");
		System.out.println("5. Viajes por estaciones de salida y llegada en un rango de tiempo (2B)");
		System.out.println("6. Hora pico de viajes por estaciones de salida y llegada (3B)");

		System.out.println("\nParte C:\n");
		System.out.println("7.  Campaña de publicidad (2C)");
		System.out.println("8.  Sectorización (3C)");
		System.out.println("9. Búsqueda georreferenciada (latitud y longitud) de estaciones");
		System.out.println("10. Búsqueda georreferenciada (latitud y longitud) de ciclorutas");
		System.out.println("11. Búsqueda de ciclorutas para hacer viaje");
		System.out.println("12. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		String[] datosFecha = fecha.split("/");


		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[1]);
		int dia = Integer.parseInt(datosFecha[0]);

		return LocalDate.of(agno, mes, dia);
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[1]);
		int dia = Integer.parseInt(datosFecha[0]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}


}
